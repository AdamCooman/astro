import { defineConfig } from "astro/config"
import svelte from "@astrojs/svelte"
// the sitemap helps google to scan the website
import sitemap from "@astrojs/sitemap"
// this recognizes $$ an $ for mathematical equations
import remarkMath from "remark-math"
// this does the rendering of the equations
import rehypeMathjax from "rehype-mathjax"

export default defineConfig({
  
  site: "https://adamcooman.gitlab.io",
  base: "/astro",
  
  integrations: [
    svelte(),
    sitemap(),
	],
	
  // we enable remarkMath and use rehypeKatex to get 
  // rendering of mathematical equations in markdown files 
  markdown: {
		remarkPlugins: [remarkMath],
        rehypePlugins: [rehypeMathjax]
    },

  // GitLab Pages requires exposed files to be located in a folder called "public".
  // So we're instructing Astro to put the static build output in a folder of that name.
  outDir: "public",
  // The folder name Astro uses for static files (`public`) is already reserved 
  // for the build output. So in deviation from the defaults we're using a folder
  // called `static` instead.
  publicDir: "static",
});
