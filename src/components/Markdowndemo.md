## 🚀 Project Structure

Inside of your Astro project, you'll see the following folders and files:

```
/
├── static/
│   ├── robots.txt
│   └── favicon.ico
├── src/
│   ├── components/
│   │   └── Tour.astro
│   └── pages/
│       └── index.astro
└── package.json
```

Astro looks for `.astro` or `.md` files in the `src/pages/` directory.
Each page is exposed as a route based on its file name.

There's nothing special about `src/components/`, but that's where we like to put any Astro/React/Vue/Svelte/Preact components.

Any static assets, like images, can be placed in the `static/` directory.

$$
y(t) = \int_0^t x(t) h(t-\tau) d\tau
$$